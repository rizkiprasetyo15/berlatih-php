<?php
function tentukan_nilai($number)
{
    //  kode disini
    $output = "";
    if ($number >= 98 && $number = 100){
        $output .= "Sangat Baik";
    } else if ($number >= 68 && $number <=76) {
        $output .= "Baik";
    } else if ($number >= 44 && $number <=67) {
        $output .= "Cukup";
    } else {
        $output .= "Kurang";
    }
    return $output;
}

//TEST CASES
echo tentukan_nilai(98) . "<br>"; //Sangat Baik 
echo tentukan_nilai(76) . "<br>"; //Baik
echo tentukan_nilai(67) . "<br>"; //Cukup
echo tentukan_nilai(43); //Kurang
?>